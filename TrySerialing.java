//Java program to demonstrate serialise and deserialise a Object
//for various different dependencies
//@author- Rahul Gautam

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

class def implements Serializable 
{
	int a;
	def(int a)
	{
		this.a=a;
	}
	public String toString() {
		return " num ="+this.a ;
	}

}
 class abc implements Serializable {

 	int a=0;
 	String name;
 	int age;
 	String gender;
 	def d ;	//compostion

 	abc( String name, int age , String gender, int i)
 	{

 		this.name = name;
 		this.age = age;
 		this.gender = gender;
 		d = new def(i);
 		
 	}

 	public String toString() {
		return "Name=" + this.name + " Age=" + this.age + " Gender=" + this.gender+ d.toString();
	}

}
//Inherinace
class xyz extends abc{
	int x;
	int y;

	xyz(int x, int y,String name, int age , String gender, int i ){

		super(name, age, gender,i);
		this.x= x;
		this.y=y;

	}

	public String toString() {
		return " x= " + this.x + " y= " + this.y + super.toString();
	}
}



class TrySerializing {
	
	public static void main(String[] args) {


		xyz obj = new xyz(100,200,"rahul", 21,"male",10);
		//Serializing
		try {
			
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("text.txt")); //writing to a text file
			oos.writeObject(obj);
			System.out.println("Done");
			oos.close();
			//fos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		//Deserializing
		try {

			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("text.txt")); 	//Reading from a text file

			abc obj2 = (abc) ois.readObject();
			ois.close();
			System.out.println(obj2.toString());
		} 
		catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	
}
