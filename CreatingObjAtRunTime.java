//A program to demonstrate dynamic loading of classes
//(Loading of classes at run time) abd their introspection using Reflection
//@author- Rahul Gautam
import java.lang.reflect.Constructor;
import java.util.Scanner;
import java.lang.reflect.InvocationTargetException;


 class CreatingObjAtRunTime {

    public void initiate(String[] args) {
        Scanner in = new Scanner(System.in);
        Class cls=this.getClass();                  `                               // loading the class to create its object dynamically
        Object O=this;
        for (String op : args) {
            try {
                cls = Class.forName(op);
            } catch (ClassNotFoundException cnfe) {
                System.out.println("Wrong Class Name");
            }
            try {
                Constructor[] cons = cls.getConstructors();                         //getting all the constructors of classes whos object is to be created
                System.out.println("Select Constructor: (Y for using N for rejecting)");
                int i = 0;
                    System.out.println(cons.length);

                while (i < cons.length) {
                    System.out.println("Select which Constructor you want to create instance of class with:");
                    System.out.println((i + 1) + ". " + cons[i]);
                    if (in.next().equalsIgnoreCase("Y")) {
                        Constructor con = cons[i];
                        Class[] c = con.getParameterTypes();                        //getting the array of type of constuctor parameters
                        i = 0;
                        System.out.println("Enter values for Parameter List: ");
                        Object[] arr=new Object[c.length];                          
                        while (i<c.length) {                                        //getting the values for the parameters of the constructor
                            System.out.println(c[i]);
                            arr[i]=in.next();
                            i++;
                        }
                        O=create(in,c,cls,arr,con);             //Calling the create function and suppling the required parameters
                        //O=con.newInstance(arr);
                        System.out.println(O);
                        break;
                    }
                    i++;
                }
            } catch (Exception cnfe) {
                System.out.println(""+cnfe.getMessage());
            }
        }
    }
    public Object create(Scanner in,Class[] param,Class cls, Object[] c,Constructor con)throws ClassNotFoundException,
            InstantiationException, IllegalAccessException,
            NoSuchMethodException, SecurityException, IllegalArgumentException,
            InvocationTargetException {
       
         System.out.println("Create");  
        Object[] obj = c;// for method1()
        
        Class params[] = param;
        for (int i = 0; i < obj.length; i++) {
        String name=params[i].getName();
        System.out.println(name);
        if (name .equals("int") ){
        obj[i]=Integer.parseInt(obj[i].toString());
                params[i] = Integer.TYPE;
            } else if (name.equals("java.lang.String")) {
            System.out.println("Param traverse");
                params[i] = String.class;
            }
            // you can do additional checks for other data types if you want.
        }

        
        Object cObject=con.newInstance(obj);                                //Creating the object
        return cObject;                                                     //Return the object created
    }
}
